export function exportExcel(data, fileName) {
    const content = data
    const blob = new Blob([content], {
        type: "application/vnd.ms-excel:chartset=UTF-8"
    })
    // const fileName = date(new Date(), 'yyyyMMddhhmmss') + '.xlsx'
    if ('download' in document.createElement('a')) {
        // 非IE下载
        const elink = document.createElement('a')
        elink.download = fileName  //命名下载名称
        elink.style.display = 'none'
        elink.href = URL.createObjectURL(blob) //表示一个指定的file对象或Blob对象
        document.body.appendChild(elink)
        elink.click()  //点击触发下载
        URL.revokeObjectURL(elink.href) // 释放URL 对象
        document.body.removeChild(elink)
    } else {
        // IE10+下载
        navigator.msSaveBlob(blob, fileName)
    }
}
