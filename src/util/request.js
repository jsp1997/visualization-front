import axios from 'axios'
import Element from "element-ui"

const request = axios.create({
    baseURL: "/api",
    timeout: 30000
})

request.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/json;charset=UTF-8';
    config.headers['Authorization'] = localStorage.getItem("token")
    return config;
}, error => {
    return Promise.reject(error);
})

request.interceptors.response.use(response => {
    const res = response.data
    // 如果是返回的文件
    if (response.config.responseType === 'blob') {
        return res
    }
    if (res.code === 200) {
        return response
    } else if (res.code === 400) {
        return res
    } else if (res.code === 500) {
        this.$notify({
            title: "系统异常",
            message: res.message,
            type: "error",
            position: "bottom-right",
            duration: 2000,
        });
    }
},
    error => {
        console.log('err' + error) // for debug
        Element.Notification({
            title: "异常",
            message: "系统异常",
            type: "error",
            position: "bottom-right",
            duration: 4000,
        });
        return Promise.reject(error)
    })

export default request;