import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home'
import SellerPage from '@/views/SellerPage'
import TrendPage from '@/views/TrendPage'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    meta: {
      title: '首页',
      roles: ["student", "teacher"],
      keepAlive: false
    }
  },
  {
    path: '/teacher',
    name: 'teacher',
    component: () => import('@/views/ToTeacher'),
    meta: {
      title: '学生列表',
      roles: ["teacher"],
      keepAlive: true
    }
  },
  {
    path: '/average',
    name: 'average',
    component: () => import('@/views/AveragePage'),
    meta: {
      title: '...',
      roles: ["student"]
    }
  },
  {
    path: '/login',
    component: () => import('@/views/Login'),
    meta: {
      title: '登录',
      roles: ["student", "teacher"],
      keepAlive: false
    }
  },
  {
    path: '/*',
    component: () => import('@/views/404'),
    meta: {
      title: '错误',
      roles: ["student", "teacher"]
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.path == '/login') {
    next()
  } else {
    const token = localStorage.getItem("token")
    if (!token) {
      next({ path: '/login' })
    }
  }

  if (null !== JSON.parse(localStorage.getItem("userInfo"))) {
    const role = JSON.parse(localStorage.getItem("userInfo")).roles[0]
    if (role === undefined || role === null) {
      return;
    }
    if (to.meta.roles.includes(role)) {
      next()
    } else {
      next({ path: "/404" })
    }
  } else {
    return;
  }

  if (to.meta.title) {
    document.title = to.meta.title
  } else {
    document.title = to.path
  }
  next()
})

export default router
