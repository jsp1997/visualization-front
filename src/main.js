import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入全局样式
import './assets/css/global.less'

// 引入重置样式
import './assets/css/dist.less'

import './assets/font/iconfont.css'
//引入font-awesome
import 'font-awesome/css/font-awesome.css'

// 引入echarts
import * as echarts from 'echarts';
Vue.prototype.$echarts = echarts

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);

require('./assets/theme/chalk')
require('./assets/theme/vintage')

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
