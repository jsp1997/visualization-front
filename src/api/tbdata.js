import request from '@/util/request'

export function seller(stuNo) {
    return request({
        url: '/score/stuScorePmt',
        method: 'get',
        params: { stuNo: stuNo }
    });
}

export function hot(stuNo) {
    return request({
        url: '/score/stuScoreZxt',
        method: 'get',
        params: { stuNo: stuNo }
    });
}

export function average(stuNo) {
    return request({
        url: '/score/stuScoreAvg',
        method: 'get',
        params: { stuNo: stuNo }
    });
}

export function queryAll(yearCode, semesterCode) {
    return request({
        url: '/teacher/query/queryAll',
        method: 'get',
        params: { yearCode: yearCode, semesterCode: semesterCode }
    });
}

export function queryClass(param) {
    return request({
        url: '/teacher/query/queryClass',
        method: 'get',
        params: param
    });
}

export function querySubject(stuNo) {
    return request({
        url: '/subject/querySub',
        method: 'get',
        params: {stuNo: stuNo}
    });
}