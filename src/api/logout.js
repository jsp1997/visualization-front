import request from '@/util/request'

export function logout() {
    return request({
        url: '/logout',
        method: 'post'
    });
}