import request from '@/util/request'

export function queryByUser(stuNo) {
    return request({
        url: '/student/queryInfo',
        method: 'get',
        params: { stuNo: stuNo }
    });
}