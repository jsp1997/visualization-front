import request from '@/util/request'

export function queryStu(teaNo) {
    return request({
        url: '/teacher/studentList',
        method: 'get',
        params: { teaNo: teaNo }
    });
}

export function queryStuVoList(data) {
    return request({
        url: '/teacher/studentVoList',
        method: 'post',
        data: data
    });
}

export function querySub(teaNo) {
    return request({
        url: '/teacher/subList',
        method: 'get',
        params: { teaNo: teaNo }
    });
}

export function queryMajor(teaNo, subName) {
    return request({
        url: '/teacher/majorList',
        method: 'get',
        params: { teaNo: teaNo, subName: subName }
    });
}

export function queryMajorSub(teaNo, majorName, subName) {
    return request({
        url: '/teacher/majorSubList',
        method: 'get',
        params: {
            teaNo: teaNo,
            majorName: majorName,
            subName: subName
        }
    });
}

export function exportStudentList() {
    return request({
        url: '/teacher/export',
        method: 'get',
        responseType: 'blob'
    });
}

export function getSubMajirList(teaNo) {
    return request({
        url: '/teacher/subMajorList',
        method: 'get',
        params: { teaNo: teaNo }
    });
}

export function queryClassAndSub(teaNo) {
    return request({
        url: '/teacher/query/queryClassAndSub',
        method: 'get',
        params: { teaNo: teaNo }
    });
}

export function queryYearCode(stuNo) {
    return request({
        url: '/score/queryYearCode',
        method: 'get',
        params: { stuNo: stuNo }
    });
}

export function queryStuScore(data) {
    return request({
        url: '/score/queryStuScore',
        method: 'post',
        data: data
    });
}

export function queryMajorName(tecNo) {
    return request({
        url: '/teacher/queryMajorName',
        method: 'get',
        params: { tecNo: tecNo }
    });
}

export function queryScore(data) {
    return request({
        url: '/teacher/queryScore',
        method: 'post',
        data: data
    });
}