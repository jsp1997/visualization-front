module.exports = {
    devServer: {
        port: 8099,
        // open: true,
        proxy: {
            'api': {
                target: 'http://localhost:8091',
                changeOrigin: true,
                pathRewrite: {
                    '/api': ''
                }
            }
        }
    }

}