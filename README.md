# student-achievement-visualization

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## 效果图

![image-20220411085159510](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411085159510.png)

![image-20220411084813124](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411084813124.png)

![image-20220411084833019](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411084833019.png)

![image-20220411084854471](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411084854471.png)

![image-20220411084918522](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411084918522.png)

![image-20220411084938989](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411084938989.png)

![image-20220411085000894](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411085000894.png)

![image-20220411085033965](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411085033965.png)

![image-20220411085105954](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411085105954.png)

![image-20220411085239496](https://jspaliyun1997.oss-cn-beijing.aliyuncs.com//毕设效果图/image-20220411085239496.png)
